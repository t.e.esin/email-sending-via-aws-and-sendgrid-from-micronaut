## Micronaut 3.2.3 Documentation

Send emails from micronaut framework:
https://guides.micronaut.io/latest/micronaut-email-maven-java.html

- [User Guide](https://docs.micronaut.io/3.2.3/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.2.3/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.2.3/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)


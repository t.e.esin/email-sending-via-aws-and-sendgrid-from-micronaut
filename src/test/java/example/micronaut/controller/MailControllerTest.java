package example.micronaut.controller;

import example.micronaut.cmd.EmailCmd;
import example.micronaut.service.EmailService;
import example.micronaut.service.MockEmailService;
import io.micronaut.context.ApplicationContext;
import io.micronaut.context.annotation.Property;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@MicronautTest
@Property(name = "spec.name", value = "mailcontroller")
class MailControllerTest {

    @Inject
    ApplicationContext applicationContext;

    @Inject
    @Client("/")
    HttpClient client;

    @Test
    public void mailsendInteractsOnceEmailService() {
        EmailCmd cmd = new EmailCmd();
        cmd.setSubject("Test");
        cmd.setRecipient("tesin@fmschool72.ru");
        cmd.setTextBody("Hola hola");
        System.out.print(cmd);
        HttpRequest<EmailCmd> request = HttpRequest.POST("/mail/send", cmd);
        EmailService emailService = applicationContext.getBean(EmailService.class);
        assertTrue(emailService instanceof MockEmailService);

        int oldEmailsSize = ((MockEmailService) emailService).emails.size();
        HttpResponse<?> rsp = client.toBlocking().exchange(request);

        assertEquals(HttpStatus.OK, rsp.getStatus());
        assertEquals(oldEmailsSize + 1 , ((MockEmailService) emailService).emails.size());
    }
}
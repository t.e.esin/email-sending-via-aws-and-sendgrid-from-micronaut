package example.micronaut.controller;

import example.micronaut.cmd.EmailCmd;
import example.micronaut.service.EmailService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;

import javax.validation.Valid;

@Controller("/mail")
public class MailController {

    private final EmailService emailService;

    public MailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @Post("/send")
    public HttpResponse<?> send(@Body @Valid EmailCmd cmd) {
        emailService.send(cmd);
        return HttpResponse.ok();
    }
}